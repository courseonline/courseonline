Rails.application.routes.draw do
  get 'author/index'

  resources :course
  resources :author
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
