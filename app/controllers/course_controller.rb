class CourseController < ApplicationController
    def new 
    end

    def index
        @course = Course.all
    end

    def create
        # get all data by create new project
        # and add data to it
        @course = Course.new(article_params)
        # running save command to begin create new course
        if @course.save
            # redirect_to @course
            # params[:course] = @course

            # redirect_to '/course' -- redirect to index page using route name
            # redirect to index page by their [prefix]_path
            redirect_to course_index_path
            # render :index, course:@course
        else
            render 'new'
        end
        # after save data => redirect to another page or success page
        # redirect_to @course
        # redirect to a plain page with information
        # render plain: params[:course].inspect
    end

    def show
        @course = Course.find(params[:id])
    end

    private 
        def article_params
            params.require(:course).permit(:cName, :cDes)
        end
end
