class CreateCourses < ActiveRecord::Migration[5.1]
  def change
    create_table :courses do |t|
      t.string :cName, null:false
      t.string :cDes, null:false
      t.date   :cCreatedAt
      t.string :cNote

      t.timestamps
    end
  end
end
